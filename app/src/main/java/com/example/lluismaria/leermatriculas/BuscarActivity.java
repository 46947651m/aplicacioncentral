package com.example.lluismaria.leermatriculas;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class BuscarActivity extends AppCompatActivity implements OnMapReadyCallback {

    private FirebaseDatabase database;
    private DatabaseReference mDatabase;
    private ArrayList<String> conductores = new ArrayList();
    private ArrayList<Ubicacion> ubis = new ArrayList();
    private ArrayAdapter<CharSequence> spinnerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar);
        Bundle extras = getIntent().getExtras();
        this.conductores = extras.getStringArrayList("conductores");
        spinnerAdapter =  new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, conductores);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        llenarSpinner();
    }

    public void buscar(View view){
        /*EditText editInicio = (EditText) findViewById(R.id.editText2);
        String inicio = editInicio.getText().toString();
        EditText editFin = (EditText) findViewById(R.id.editText3);
        String fin = editFin.getText().toString();*/
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        googleMap.clear();
        Spinner spinner = (Spinner)findViewById(R.id.spinner);
        String nombre = spinner.getSelectedItem().toString();
        database = FirebaseDatabase.getInstance();
        mDatabase = database.getReference("root").child(nombre);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                PolylineOptions line = new PolylineOptions();
                line.clickable(false);
                for (DataSnapshot ubiSnapshot : dataSnapshot.getChildren()) {
                    Ubicacion last = ubiSnapshot.getValue(Ubicacion.class);
                    line.add(new LatLng(last.latitud, last.longitud));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(last.latitud, last.longitud), 10));
                }
                Polyline linea = googleMap.addPolyline(line);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void llenarSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(spinnerAdapter);
        spinnerAdapter.notifyDataSetChanged();
    }
}
