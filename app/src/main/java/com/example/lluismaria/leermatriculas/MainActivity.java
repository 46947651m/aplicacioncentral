package com.example.lluismaria.leermatriculas;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity  {

    private FirebaseDatabase database;
    private DatabaseReference mDatabase;
    private ArrayList<String> conductores = new ArrayList();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        obtenerConductores();
    }

    public void verTodos(View view){
        Intent intent = new Intent(this, VerTodosActivity.class);
        intent.putStringArrayListExtra("conductores", conductores);
        startActivity(intent);
    }

    public void buscar(View view){
        Intent intent = new Intent(this, BuscarActivity.class);
        intent.putStringArrayListExtra("conductores", conductores);
        startActivity(intent);
    }

    public void obtenerConductores(){
        database = FirebaseDatabase.getInstance();
        mDatabase = database.getReference("root");
        mDatabase.addListenerForSingleValueEvent( new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot conductorSnapshot : dataSnapshot.getChildren()) {
                    conductores.add(conductorSnapshot.getKey());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}