package com.example.lluismaria.leermatriculas;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lluis Maria on 20/03/2018.
 */

public class MyAdapter extends BaseAdapter {

    private ArrayList<Ubicacion> items;
    private Context context;

    public MyAdapter (Context context,  ArrayList<Ubicacion> ubicaciones) {
        items = ubicaciones;
        this.context = context;
    }

    public void addAdapterItem(Ubicacion item) {
        items.add(item);
    }

    public int getCount() {
        return items.size();
    }

    public Ubicacion getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    View rowView = inflater.inflate(R.layout.layout, null);
    TextView tvTiempo = (TextView) rowView.findViewById(R.id.tvtiempo);
    tvTiempo.setText(String.valueOf(items.get(position).convertirTiempo()));
        TextView tvLatitud = (TextView) rowView.findViewById(R.id.tvLatitud);
        tvLatitud.setText(Ubicacion.formatear(items.get(position).latitud));
        TextView tvLongitud = (TextView) rowView.findViewById(R.id.tvLongitud);
        tvLongitud.setText(Ubicacion.formatear(items.get(position).longitud));
        Log.i("getView", "getView");

    // do the same with second and third
     return rowView;
}
}
