package com.example.lluismaria.leermatriculas;



import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Lluis Maria on 20/03/2018.
 */

public class Ubicacion {

    public double latitud;
    public double longitud;
    public long tiempo;

    public Ubicacion()
    {

    }

    public Ubicacion(double latitud, double longitud, long tiempo) {
        this.latitud = latitud;
        this.longitud = longitud;
        this.tiempo = tiempo;
    }

    public String convertirTiempo()
    {
        Date date = new Date(tiempo);
        String string_date;
        SimpleDateFormat df = new SimpleDateFormat ("dd/MM/YYYY hh:mm:ss", Locale.ENGLISH);

         string_date = df.format(date);
         return string_date;
    }

    public static String formatear (double magnitud)
    {
        DecimalFormat nf = new DecimalFormat("#.0000");
        return String.valueOf(nf.format(magnitud));
    }

}

