package com.example.lluismaria.leermatriculas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class VerTodosActivity extends AppCompatActivity implements OnMapReadyCallback {
    private FirebaseDatabase database;
    private DatabaseReference mDatabase;
    private ArrayList<String> conductores = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_todos);
        Bundle extras = getIntent().getExtras();
        this.conductores = extras.getStringArrayList("conductores");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        googleMap.clear();
        database = FirebaseDatabase.getInstance();
        for (String s : conductores) {
            Query a = database.getReference("root").child(s).orderByChild("tiempo").limitToLast(1);
            a.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Ubicacion last = dataSnapshot.getValue(Ubicacion.class);
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(last.latitud, last.longitud)).title(dataSnapshot.getKey()));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(last.latitud, last.longitud), 10));
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
/*
        mDatabase = database.getReference("root");
        mDatabase.addListenerForSingleValueEvent( new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot conductorSnapshot : dataSnapshot.getChildren()) {
                    Query a = conductorSnapshot.getRef().orderByChild("tiempo").limitToLast(1);
                    a.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Ubicacion last = dataSnapshot.getValue(Ubicacion.class);
                            googleMap.addMarker(new MarkerOptions().position(new LatLng(last.latitud, last.longitud)).title(dataSnapshot.getKey()));
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(last.latitud, last.longitud), 10));
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
    }
}
